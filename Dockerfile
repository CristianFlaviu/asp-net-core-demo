#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:3.1 AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/sdk:3.1 AS build
WORKDIR /src
COPY ["asp.net-core-demo.csproj", "."]
RUN dotnet restore "./asp.net-core-demo.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "asp.net-core-demo.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "asp.net-core-demo.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "asp.net-core-demo.dll"]